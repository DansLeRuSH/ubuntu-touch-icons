Hi everybody,

You can find here a bunch of 20 icons I did _freely_ for several apps and webapps on Ubuntu Touch.

Please use the following designs at your convenience but it would be really nice to **quote me**.

However, please keep in mind that it was just a graphical adaptation job and **I'M NOT THE OWNER OF THE LOGOS AND _TRADEMARKS_ USED**.

See Ya / Best Regards,
Franck